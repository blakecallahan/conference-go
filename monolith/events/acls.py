import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_picture(city, state):
    params = {"query": city + " " + state}
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, params=params, headers=headers)
    picture = json.loads(response.content)

    photo_dict = {"picture_url": picture["photos"][0]["src"]["original"]}
    return photo_dict


def get_lat_long(location):
    base_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{location.city},{location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    return {
        "latitude": parsed_json[0]["lat"],
        "longitude": parsed_json[0]["lon"],
    }


def get_weather_data(location):
    lat_long = get_lat_long(location)
    base_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat_long["latitude"],
        "lon": lat_long["longitude"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)

    return {
        "temp": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"],
    }
