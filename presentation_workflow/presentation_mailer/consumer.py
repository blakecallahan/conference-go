import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

def process_approval(ch, method, properties, body):
    print("  Received %r" % body)
    jbody = json.loads(body)
    send_mail(
        'Your presentation has been accepted',
        f'{jbody["presenter_name"]}, we are happy to tell you that your presentation {jbody["title"]} has been accepted. ',
        'admin@conference.go',
        [f'{jbody["presenter_email"]}'],
        fail_silently=False)


def process_rejection(ch, method, properties, body):
    print("  Received %r" % body)
    jbody = json.loads(body)
    send_mail(
        'Your presentation has been rejected',
        f'{jbody["presenter_name"]}, we are sad to tell you that your presentation {jbody["title"]} has been rejected. ',
        'admin@conference.go',
        [f'{jbody["presenter_email"]}'],
        fail_silently=False)

while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
        channel = connection.channel()

        channel.queue_declare(queue='presentation_rejection')
        channel.basic_consume(
            queue='presentation_rejection',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.queue_declare(queue='presentation_approval')
        channel.basic_consume(
            queue='presentation_approval',
            on_message_callback=process_approval,
            auto_ack=True,
        )

        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
